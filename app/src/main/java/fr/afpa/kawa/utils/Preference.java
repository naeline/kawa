package fr.afpa.kawa.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.kawa.models.Cafes;

public class Preference {
    // todo : etape 1 recuperer une instance de l objet Preference
    // todo : etape 2 add Record (café)
    // todo : etape 3 delete Record (café)
    // todo : etape 4 recuperer tout les cafés

    // Préférence est un fichier XML donc principe de clé/valeur
    //  /data/data/fr.afpa.kawa/preference.xml

    // dans cette methode on va utiliser que des static

    // todo : 1
    private static SharedPreferences getPreference(Context c) { // on met en parametre un context pour pouvoir mettre un contexte dans la methode d en dessous
        return PreferenceManager.getDefaultSharedPreferences(c);
    }

    // todo : 2
    public static boolean addCafe(Context c, Cafes.Records cafe) {
        List<Cafes.Records> recordsList = getCafes(c);

        if(!isInFavorite(recordsList, cafe.getFields().getNom_du_cafe())){  // pour eviter de mettre plusieurs fois le meme favori
            recordsList.add(cafe);

            save(c, recordsList);   // ajoute du contexte dans la methode save donc on l ajoute ici aussi
            return true;
        }
        return false;
    }

    private static void save(Context c, List<Cafes.Records> recordsList) {

        // convertion en json
        Gson myGson = new Gson();
        String json = myGson.toJson(recordsList);

        // enregistrement des informations
        getPreference(c)
                .edit()
                .putString("favorites", json)
                .commit();
    }


    // todo : 3
    public static boolean removeCafe(Context c, List<Cafes.Records> favoriteList, Cafes.Records records) { // cafes.recprds records ajouté automatiquement en ayant fait alt entrée dans favoriteActivity

        String coffeeName = records.getFields().getNom_du_cafe();


        for (int i = 0; i < favoriteList.size(); i++) {
            if (favoriteList.get(i).getFields().getNom_du_cafe().equals(coffeeName)){
                favoriteList.remove(i);
                save(c, favoriteList);
                return true;
            }
        }
        return false;
    }

    // todo : 4
    public static List<Cafes.Records> getCafes(Context c) {
        List<Cafes.Records> recordsList = new ArrayList<>();

        String json = getPreference(c).getString("favorites", null); // deuxieme valeur demandée on met null

        if (json != null) {
            Gson myGson = new Gson();
            recordsList = myGson.fromJson(json, new TypeToken<List<Cafes.Records>>(){}.getType());
        }
        return recordsList;
    }

    // réinitialiser les préférences
    // ici juste pour le developpeur a appelé dands le main on create. si on veut que l utilisateur l ai faut faire un bouton
    public static void clear(Context c) {
        getPreference(c)
                .edit()
                .clear()
                .commit();
    }

    // la méthode pour eviter de mettre plusieurs fois le meme favori
    public static boolean isInFavorite(List<Cafes.Records> favoriteList, String coffeeName){
        for (int i = 0; i < favoriteList.size(); i++) {
            if (favoriteList.get(i).getFields().getNom_du_cafe().equals(coffeeName)){
                return true;
            }
        }
        return false;
    }
}

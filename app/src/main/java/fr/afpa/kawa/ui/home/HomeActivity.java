package fr.afpa.kawa.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.mapbox.mapboxsdk.maps.MapView;

import fr.afpa.kawa.R;
import fr.afpa.kawa.models.Cafes;
import fr.afpa.kawa.ui.favorite.FavoriteActivity;
import fr.afpa.kawa.ui.home.listing.ListingFragment;
import fr.afpa.kawa.ui.home.map.MapFragment;
import fr.afpa.kawa.ui.nonetwork.NoNetworkFragment;
import fr.afpa.kawa.utils.Constant;
import fr.afpa.kawa.utils.FastDialog;
import fr.afpa.kawa.utils.Network;

public class HomeActivity extends AppCompatActivity {

    private Cafes myCoffees;
    private Button buttonMap;
    private Button buttonListing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        buttonMap = (Button) findViewById(R.id.buttonMap);
        buttonListing = (Button) findViewById(R.id.buttonListing);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (buttonListing.isActivated()) {
            fetchCafes(buttonListing);
        } else {
            fetchCafes(buttonMap);
        }
    }
            // ajout d une view en argument
    private void fetchCafes(final View view) {

        if (Network.isNetworkAvailable(HomeActivity.this)) {

            RequestQueue queue = Volley.newRequestQueue(this);
            String url = Constant.URL_CAFE;
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String json) {

                            Gson myGson = new Gson();

                            myCoffees = myGson.fromJson(json, Cafes.class);

                            Toast.makeText(
                                    HomeActivity.this,
                                    "Un petit café à 1 € ?",
                                    Toast.LENGTH_LONG
                            ).show();

                            view.performClick();

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // Handle error
                        }
                    });

// Add the request to the RequestQueue.
            queue.add(stringRequest);
        } else {
            FastDialog.showDialog(HomeActivity.this, FastDialog.SIMPLE_DIALOG, "Vous devez être connecté");
            switchFragment(new NoNetworkFragment());
        }
    }

    // methode pour faire apparaitre la carte au click bouton carte
    public void showMap(View view) {

        // pour eviter de pouvoir rappuyer plusieurs fois
        if (buttonMap.isActivated()) {
            return;
        }
        // pour avoir le bouton avec le liseré du bat actif
        // et enlever celui de l'autre bouton
        buttonMap.setActivated(true);
        buttonListing.setActivated(false);

        //changement de couleur de texte
        // contextCompat permet de gérer la rétrocompatibilité de certaines fonctions qui ont changé
        buttonMap.setTextColor(ContextCompat.getColor(HomeActivity.this, android.R.color.black));
        buttonListing.setTextColor(ContextCompat.getColor(HomeActivity.this, android.R.color.darker_gray));

        if (myCoffees != null) {
            switchFragment(MapFragment.newInstance(myCoffees));
        } else {
            fetchCafes(buttonMap);
        }
    }

    //methode pour faire apparaitre la liste au click bouton liste
    public void showListing(View view) {

        if (buttonListing.isActivated()) {
            return;
        }

        buttonListing.setActivated(true);
        buttonMap.setActivated(false);

        //changement de couleur de texte
        buttonListing.setTextColor(ContextCompat.getColor(HomeActivity.this, android.R.color.black));
        buttonMap.setTextColor(ContextCompat.getColor(HomeActivity.this, android.R.color.darker_gray));

        if (myCoffees != null) {
            switchFragment(ListingFragment.newInstance(myCoffees));
        } else {
            fetchCafes(buttonListing);
        }
    }

    // methode pour eviter de faire 2 methoders differentes la on a la methode switchfragment et on a juste changé le parametre
    private void switchFragment(Fragment fragment) {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameLayoutContainerFragment, fragment)
                .commit();
    }


    public void showFavorite(View view) {

        Intent intentFavorite = new Intent(HomeActivity.this, FavoriteActivity.class);
        startActivity(intentFavorite);
    }
}

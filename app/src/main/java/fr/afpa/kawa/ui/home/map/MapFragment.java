package fr.afpa.kawa.ui.home.map;


import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import java.util.List;

import fr.afpa.kawa.R;
import fr.afpa.kawa.models.Cafes;

/**
 * A simple {@link Fragment} subclass.
 */
// Généré quand on fait new fragment blank
    // fragment comme une iframe. Partie qui charge une autre page
public class MapFragment extends Fragment {

    private static final String PARAM_OBJECT_COFFEE = "PARAM_OBJECT_COFFEE";

    private Cafes myCofees; // aucun lien avec le myCoffees de home juste pour savoir qu on parle de la meme chose

    private MapView mapView;

    public MapFragment() {
        // Required empty public constructor
    }

    public static MapFragment newInstance(Cafes myCoffees) {    // methode importé grace a alt entree sur le homeactivity, Fragment remplacé par MapFragment
        MapFragment fragment = new MapFragment();

        Bundle myBundle = new Bundle(); // bundle stocke des données
        myBundle.putSerializable(PARAM_OBJECT_COFFEE, myCoffees); // methode qui transforme l objet en caractere
        fragment.setArguments(myBundle);    // on utilise setArguments car on a une constante definie avec des arguments. A ce moment la les cafés sont dans le frament

        return fragment;
    }

    // creation de methode onCreate
    // on evite de le mettre dans onCreateView car s il y a un destroy de onCreateView on va devoir refaire cette manip hors que la elle sera faite qu une fois
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            myCofees = (Cafes) getArguments().get(PARAM_OBJECT_COFFEE); // alt entree sur la constante pour la caster et ajout de (Cafes)
        }
    }

    /***************
    **** METHODE DONNEE PAR MAPBOX SUR LE SITE REAJUSTE POUR LE FRAGMENT***
     *******************/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // on a remplace this par getContext car on est dans un fragment( permet de récupérer le context de ce qui va charger le fragment) et accestoken donné directement
        Mapbox.getInstance(getContext(), "pk.eyJ1IjoibmFlbGluZSIsImEiOiJjanN2dWd4ZjQwNDVkNGFvZnBxb2F2a2gyIn0.4foGqHAPeWJ33QsEd4p90A");
        // Inflate the layout for this fragment
        // ici on a creer une variable view framentView pour pouvoir faire un findview par la suite puis le return a la fin
        View fragmentView = inflater.inflate(R.layout.fragment_map, container, false);

        mapView = fragmentView.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap mapboxMap) {
                mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        // Map is set up and the style has loaded. Now you can add data or make other map adjustments

                        List<Cafes.Records> records = myCofees.getRecords();
                        for (int i = 0; i < records.size(); i++) {
                            Cafes.Fields fields = records.get(i).getFields();

                            // Add the marker image to map
                            style.addImage("marker-icon-id"+i,
                                    BitmapFactory.decodeResource(
                                            getResources(), R.drawable.icon_marker)); // ici on modifie le marker par celui qu on a mis dans le mdpi


                            GeoJsonSource geoJsonSource = new GeoJsonSource("source-id" + i, Feature.fromGeometry(
                                    Point.fromLngLat(fields.getGeoloc()[1], fields.getGeoloc()[0])));
                            style.addSource(geoJsonSource);

                            SymbolLayer symbolLayer = new SymbolLayer("layer-id" + i, "source-id" + i);
                            symbolLayer.withProperties(
                                    PropertyFactory.iconImage("marker-icon-id" + i)
                            );
                            style.addLayer(symbolLayer);
                        }
                    }

                });
            }
        });
        return fragmentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroyView() {   // methode ici modifié de onDestroy par onDestroyView pour pouvoir passer d un fragment a l autre
        super.onDestroyView();      // pareil ici
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);

    }

}

package fr.afpa.kawa.ui.home.listing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;


import java.util.List;

import fr.afpa.kawa.R;
import fr.afpa.kawa.models.Cafes;
import fr.afpa.kawa.utils.Preference;

public class CafeAdapter extends ArrayAdapter<Cafes.Records> {   // entre chevrons il attend l objet qui sera répétée la partie list5days ici

    private int resId;

    public CafeAdapter(Context context, int resource, List<Cafes.Records> objects) {

        super(context, resource, objects);

        resId = resource;   // R.layout.item_city on le créer au dessus pour le réutiliser pour aller plus vite

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        convertView = LayoutInflater.from(getContext()).inflate(resId, null);
        TextView textViewName = convertView.findViewById(R.id.textViewName);
        TextView textviewArrondissement = convertView.findViewById(R.id.textViewArrondissement);

        ImageButton imageButtonStar = convertView.findViewById(R.id.imageButtonStar);

        Cafes.Records item = getItem(position);    // via la variable item que l'on va récuperer les elements de Openwlist5days

        // affichage de la date, temperature et image
        textViewName.setText(item.getFields().getNom_du_cafe());
        textviewArrondissement.setText(item.getFields().getArrondissement());

        List<Cafes.Records> cafeFavorites = Preference.getCafes(getContext());
        if (!Preference.isInFavorite(cafeFavorites, item.getFields().getNom_du_cafe())) {
            imageButtonStar.setAlpha(0.005f);
        }


        return convertView;
    }
}

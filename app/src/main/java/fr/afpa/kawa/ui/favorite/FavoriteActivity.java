package fr.afpa.kawa.ui.favorite;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import fr.afpa.kawa.R;
import fr.afpa.kawa.models.Cafes;
import fr.afpa.kawa.ui.home.listing.CafeAdapter;
import fr.afpa.kawa.utils.Preference;

public class FavoriteActivity extends AppCompatActivity {

    private ListView listViewCoffee;
    private List<Cafes.Records> cafesList;
    private CafeAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        cafesList = Preference.getCafes(FavoriteActivity.this); // alt entree create field pour le declarer au dessus. cafesList fait apres pour le mettre plus bas
        listViewCoffee = (ListView) findViewById(R.id.listViewCoffee);

        adapter = new CafeAdapter(FavoriteActivity.this,
                R.layout.item_coffee,
                cafesList
        );

        listViewCoffee.setAdapter(adapter);

        listViewCoffee.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                Preference.removeCafe(FavoriteActivity.this, cafesList, cafesList.get(position));   // suppression du cafe
                adapter.notifyDataSetChanged(); // rafraichissement de la page
                return false;
            }
        });
    }

}

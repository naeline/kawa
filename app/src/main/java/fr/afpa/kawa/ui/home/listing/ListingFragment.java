package fr.afpa.kawa.ui.home.listing;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.List;

import fr.afpa.kawa.R;
import fr.afpa.kawa.models.Cafes;
import fr.afpa.kawa.ui.home.HomeActivity;
import fr.afpa.kawa.ui.home.map.MapFragment;
import fr.afpa.kawa.utils.Preference;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListingFragment extends Fragment {

    private ListView listViewCoffee;


    private static final String PARAM_OBJECT_COFFEE = "PARAM_OBJECT_COFFEE";

    private Cafes myCofees; // aucun lien avec le myCoffees de home juste pour savoir qu on parle de la meme chose
    private CafeAdapter adapter;

    @Override
    public void onResume() {
        super.onResume();

        //acutalisation
        adapter.notifyDataSetChanged();
    }

    public static ListingFragment newInstance(Cafes myCoffees) {    // methode importé grace a alt entree sur le homeactivity, Fragment remplacé par MapFragment
        ListingFragment fragment = new ListingFragment();

        Bundle myBundle = new Bundle(); // bundle stocke des données
        myBundle.putSerializable(PARAM_OBJECT_COFFEE, myCoffees); // methode qui transforme l objet en caractere
        fragment.setArguments(myBundle);    // on utilise setArguments car on a une constante definie avec des arguments. A ce moment la les cafés sont dans le frament

        return fragment;
    }

    // creation de methode onCreate
    // on evite de le mettre dans onCreateView car s il y a un destroy de onCreateView on va devoir refaire cette manip hors que la elle sera faite qu une fois
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            myCofees = (Cafes) getArguments().get(PARAM_OBJECT_COFFEE); // alt entree sur la constante pour la caster et ajout de (Cafes)
        }
    }

    // A NE SURTOUT PAS SUPPRIMER
    public ListingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentViewList = inflater.inflate(R.layout.fragment_listing, container, false);

        List<Cafes.Records> records = myCofees.getRecords();
        adapter = new CafeAdapter(getContext(), R.layout.item_coffee, records);


        listViewCoffee =fragmentViewList.findViewById(R.id.listViewCoffee);

        listViewCoffee.setAdapter(adapter);


        listViewCoffee.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Cafes.Records cafe = myCofees.getRecords().get(position);
                if (Preference.addCafe(getContext(), cafe)) {
                    Toast.makeText(getContext(), "Café ajouté en favoris", Toast.LENGTH_SHORT).show();
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getContext(), "Ce café est déjà en favoris", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        return fragmentViewList;
    }

}

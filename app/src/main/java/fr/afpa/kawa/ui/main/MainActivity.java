package fr.afpa.kawa.ui.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import fr.afpa.kawa.BuildConfig;
import fr.afpa.kawa.R;
import fr.afpa.kawa.ui.home.HomeActivity;
import fr.afpa.kawa.utils.Preference;

public class MainActivity extends AppCompatActivity {

    // Initialisation de variable
    private Timer myTimer;

    @Override
    // généré par l ide en tapant entrer.    OVERRIDE = SURCHARGE
    protected void onCreate(Bundle savedInstanceState) {// Lors de l'héritage récupération des données maiq qu'on peut modifier
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // quand on sera en mode debug les favoris seront effacés
        if (BuildConfig.DEBUG) {
            Preference.clear(MainActivity.this);
        }

        myTimer = new Timer();                          // instance de classe
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Log.e("Main", "Lancer HomeActivity");   // afficher dans la console logcat en erreur vu que le e

                Intent myIntent = new Intent(MainActivity.this, HomeActivity.class);   // INTENT est une class qui sert de paramétrage
                startActivity(myIntent);        // demarre l'INTENT     // si on veut récupérer la class dans la quelle on est plutot mettre son nom.this que this tout court car sinon le this récupère l objet dans lequel il est directement implémenté
                finish(); // permet de sortir de l'appli quand on revient sur le premier écran autre maniere dans le manifest mettre android:noHistory="true" pour la page d accueil
            }
        }, 2000);
    }
}

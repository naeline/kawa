package fr.afpa.kawa.models;

import java.io.Serializable;
import java.util.List;

public class Cafes implements Serializable {    // implémenté en ayant fait alt entrée sur mycofees ans MapFragment dans la methode MapFragment
    public List<Records> getRecords() {
        return records;
    }

    private List<Records> records;  // dans le fichier jSon la liste records

    public class Records implements Serializable {  // on ajoute l implements pour pouvoir aller dans les favoris sans que ca bug
        private Fields fields;

        public Fields getFields() {
            return fields;
        }
    }

    public class Fields implements Serializable {   // on ajoute l implements pour pouvoir aller dans les favoris sans que ca bug
        private String arrondissement;  // dans le jSon la donnée arrondissement
        private String adresse;         // dans le jSon la donnée adresse
        private String nom_du_cafe;     // dans le jSon la donnée nom_du_cafe
        private String prix_terasse;    // dans le jSon la donnée
        private String prix_comptoir;   // dans le jSon la donnée
        private String prix_salle;      // dans le jSon la donnée
        private double[] geoloc;        // dans le jSon recupere le tableau geoloc, on a des chiffres a virgule donc double

        public String getArrondissement() {
            return arrondissement;
        }

        public String getAdresse() {
            return adresse;
        }

        public String getNom_du_cafe() {
            return nom_du_cafe;
        }

        public String getPrix_terasse() {
            return prix_terasse;
        }

        public String getPrix_comptoir() {
            return prix_comptoir;
        }

        public String getPrix_salle() {
            return prix_salle;
        }

        public double[] getGeoloc() {
            return geoloc;
        }
    }

}
